
var chosenCar=-1;
var numGrades=-1;
var dataAll;

//загрузка данных их БД
$.get( "/cars", function( data ) {
 	for(i=0;i<data.length;i++){
     	var str=$("<li><a num-car='"+i+"'>"+data[i][Object.keys(data[i])[1]].title+"</a></li>");
     	$(".drop1").append(str);
    }

    dataAll=data;
});

//Изменение текста кнопки выпадающего списка
function changeButtonText(currentThis,id){
	var caret=$("<span class='caret'></span>");
	if(typeof(currentThis)!="string"){
	   	$(id).text($(currentThis).text()+' ');
	    $(id).val($(currentThis).text());
	}
	else{
		$(id).text(currentThis+' ');
	    $(id).val(currentThis);
	}
    $(id).append(caret);
}

var techSpecMas;
//Обработка выпадающего списка "Выбор Модели"
$(".drop1").on('click', 'li a', function(){

	changeButtonText('Выбор комплектации',"#dropdownMenu2")
	$('.table tbody').remove();
	$('.drop2 li').remove();


   	changeButtonText(this,"#dropdownMenu1");
   	chosenCar=$(this).attr("num-car");
   	$("#dropdownMenu1").attr("num-car",chosenCar);
    for(var j=0;j<dataAll[chosenCar][Object.keys(dataAll[chosenCar])[1]].grades.length;j++){
     	var str2=$("<li><a num-grades='"+j+"'>"+dataAll[chosenCar][Object.keys(dataAll[chosenCar])[1]].grades[j].engine_desc+"</a></li>");
     	$(".drop2").append(str2);
    }

});

//Обработка выпадающего списка "Выбор Комплектации"
$(".drop2").on('click','li a',function(){
	changeButtonText(this,"#dropdownMenu2");
	$('.table tbody').remove();
	if(chosenCar!=-1){
		numGrades=$(this).attr("num-grades");
   		techSpecMas=dataAll[chosenCar][Object.keys(dataAll[chosenCar])[1]].grades[numGrades].technicalSpecification;
   		for(i=0;i<techSpecMas.length;i++){
   			var str=$("\
   				<tr>\
   					<td><input type='checkbox' value='"+i+"'/></td>\
   					<td>"+techSpecMas[i].title+"</td>\
   					<td>"+techSpecMas[i].details+"</td>\
   					<td>"+techSpecMas[i].type+"</td>\
   				</tr>\
   				");
     		$(".table").append(str);
   		}
   	}
});


function createResult(){//выбранные checkbox, которые нужно отправить
	var numtechSpec=[];
	for(var i=0;i<$("input:checkbox:checked").length;i++){
		numtechSpec.push($("input:checkbox:checked")[i].value);
	}
	return numtechSpec;
}



//Обработка отправки на почту
$("#ajaxform").submit(function(){ 
	var chosenNums=createResult();
	$.post("/sendEmail", {email:$(".email").val(),chosenNums,chosenNumCar:chosenCar,chosenNumGrades:numGrades}, function( data ) {
		console.log(data);
		if(data.yo === 'error'){
			alert("Не удалось оправить письмо.");
		}else{
			alert("Письмо с характеристиками отправлено на почту!");
		}
	     
	});
	return false;
});