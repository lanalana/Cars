//БД
var MongoClient = require('mongodb').MongoClient
  , assert = require('assert');

// Connection URL
var url = 'mongodb://localhost:27017/testDB';


var express = require('express');
var app = express();
app.use(express.static('public'));

var nodemailer = require('nodemailer');

//Для получения данных от клиента
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

//чтение json фвйла
var fs = require('fs');
var data = fs.readFileSync('all.json', 'utf8');
var obj = JSON.parse(data);

	for(i=0;i<obj.length;i++){
			const car = obj[i];
			var cari = car[Object.keys(car)[0]];
      for(var j=0;j<cari.grades.length;j++){
  			delete cari.grades[j].accessories;
  			delete cari.grades[j].options;
      }
	}


var insertDocuments = function(db, callback) {
  db.collection('cars').drop();
  // Get the documents collection
  var cars = db.collection('cars');
  // Insert some documents
  cars.insertMany(obj, function(err, result) {
    console.log("Inserted documents into the collection",err);
    findDocuments(db, function(docs) { //console.log(docs);
     });
    callback(result);
  });
}



var findDocuments = function(db, callback) {
  // Get the documents collection
  var cars = db.collection('cars');
  // Find some documents
  cars.find({}).toArray(function(err, docs) {
    // assert.equal(err, null);
    console.log("Found the following records");
    callback(docs);
  });
}



// Use connect method to connect to the server
MongoClient.connect(url, function(err, db) {

  assert.equal(null, err);
  console.log("Connected successfully to server");

  insertDocuments(db, function() {
  });

//Отправка данных Клиенту
  app.get('/cars', function (req, res) {
        findDocuments(db, function(docs) {
          res.send(docs);
        });
  });


//отправка e-mail
  app.post('/sendEmail', function (req, res) {//Получение данных от клиента о выбранных характеристиках
    // console.log(req.body);

    //подготовка текса письма
    var message='';
    var car;
    var carName;
    var engine_desc;
    findDocuments(db, function(docs) {
        car=docs[req.body.chosenNumCar][Object.keys(docs[req.body.chosenNumCar])[1]];
        carName=car.title;

        engine_desc=car.grades[req.body.chosenNumGrades].engine_desc;
        console.log(engine_desc);
        for(var i=0;i<req.body.chosenNums.length;i++){
            var techSpec=car.grades[req.body.chosenNumGrades].technicalSpecification[req.body.chosenNums[i]];
            message=message+i+') '+techSpec.title+';'+techSpec.details+';'+techSpec.type+'\n';
        }

        console.log(message);

      //Собственно отправка
      var mailOptions = {
        from: 'example@gmail.com', // sender address
        to: req.body.email, // list of receivers
        subject: 'Технические характеристики '+carName+' Комплектации: '+engine_desc, // Subject line
        text: 'Технические характеристики '+carName+' Комплектации: '+engine_desc+':\n'+message //, // plaintext body
       // html: '<b>Hello world ✔</b>' // You can choose to send an HTML body instead
      };

      var transporter = nodemailer.createTransport({
          service: 'Gmail',
          auth: {
              user: 'testEmailforCars@gmail.com', // Your email id
              pass: 'west1010' // Your password
          }
      });
      transporter.sendMail(mailOptions, function(error, info){
          if(error){
              console.log(error);
              res.send({yo: 'error'});
          }else{
              console.log('Message sent: ' + info.response);
              res.json({yo: info.response});
          };
      });
    });
    
  });

  app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
  });
});









